package ch5;

import ch2.Widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

public class VectorIteration<E> {

    private final Vector<E> vector;
    private final List<? extends Widget> list;

    public VectorIteration(Vector<E> vector) {
        this.vector = vector;
        this.list = Collections.synchronizedList(new ArrayList<Widget>());
    }

    /*
    Iteration that may throw index out of bounds exception even though vector is thread-safe
     */
    private void iterate(Vector<E> vector) {
        for (E e : vector) {
            System.out.println(e);
        }
    }

    private void threadSafeIterate(Vector<E> vector) {
        synchronized (vector) {
            for (E e : vector) {
                System.out.println(e);
            }
        }
    }

    private void synchronizedListIteration(List<? extends Widget> list) {
        for (Widget w : list) {
            w.doSomething();
        }
    }


}
