package modern.java.in.action.ch17;

import java.util.concurrent.Flow.*;

public class Main {

    public static void main(String[] args) {
        // getTemperatures("New York").subscribe(new TempSubscriber());
        getCelsiusTemperatures("LA").subscribe(new TempSubscriber());
    }

    public static Publisher<TempInfo> getTemperatures(String town) {
        return subscriber -> subscriber.onSubscribe(new TempSubscription(subscriber, town));
    }

    public static Publisher<TempInfo> getCelsiusTemperatures(String town) {
        return subscriber -> {
            TempProcessor processor = new TempProcessor();
            processor.subscribe(subscriber);
            processor.onSubscribe(new TempSubscription(processor, town));
        };
    }
}
