package nutshell;

public class Counter {

    private int i = 0;

    public int increment() {
        return i++;
    }

    public static void main(String[] args) throws InterruptedException {
        Counter c = new Counter();
        int REPEAT = 10_000_000;
        Runnable r = () -> {
            for (int i = 0; i < REPEAT; i++) {
                c.increment();
            }
        };
        Thread t1 = new Thread();
        Thread t2 = new Thread();

        t1.start();
        t2.start();
        t1.join();
        t2.join();


        int anomaly = (2 * REPEAT) - c.increment();
        double perc = ((anomaly + 0.0) * 100) / (2 * REPEAT);
        System.out.println("Lost updates: " + anomaly +" ; % = " + perc);
    }
}
