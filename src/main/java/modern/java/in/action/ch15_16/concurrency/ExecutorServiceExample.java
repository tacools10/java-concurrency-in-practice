package modern.java.in.action.ch15_16.concurrency;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecutorServiceExample {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        int x = 1337;

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Future<Integer> y = executorService.submit(() -> f(x));
        Future<Integer> z = executorService.submit(() -> g(x));
        System.out.println(y.get() + z.get());

        executorService.shutdown();
    }

    private static int f(int x) {
        return Math.addExact(x, 500);
    }

    private static int g(int x) {
        return Math.subtractExact(x, 1000);
    }

    private static class Result {
        private int left;
        private int right;
    }
}
