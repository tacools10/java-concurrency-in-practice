package modern.java.in.action.ch17.rxjava;

import io.reactivex.Observable;
import modern.java.in.action.ch17.TempInfo;

import static modern.java.in.action.ch17.rxjava.TempObservable.getTemperature;

public class Main {

    public static void main(String[] args) {
        Observable<TempInfo> observable = getTemperature("New York");
        observable.subscribe(new TempObserver());

        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }



}
