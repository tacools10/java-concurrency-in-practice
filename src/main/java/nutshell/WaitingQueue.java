package nutshell;

import java.util.LinkedList;
import java.util.List;

public class WaitingQueue<E> {

    LinkedList<E> queue = new LinkedList<E>();

    public synchronized void push(E o) {
        queue.add(o);
        this.notifyAll();
    }

    public synchronized E pop() {
        while (queue.size() == 0) {
            try {
                this.wait();
            } catch (InterruptedException ignore) {

            }
        }
        return queue.remove();
    }
}
