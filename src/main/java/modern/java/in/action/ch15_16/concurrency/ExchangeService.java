package modern.java.in.action.ch15_16.concurrency;

public class ExchangeService {

    public static void delay() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public Double getRate(String currToConvertFrom, String currToConvertTo) {
        if (currToConvertFrom.toLowerCase().equals(currToConvertTo.toLowerCase())) {
            return 1.00;
        }
        delay();
        return 1.50;
    }


}
