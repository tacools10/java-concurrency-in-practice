package ch3;

/**
 * StuffIntoPublic
 * <p/>
 * Unsafe publication
 *
 * @author Brian Goetz and Tim Peierls
 */
public class StuffIntoPublic {

    /* Holder field is not final and is not immutable,
        so it is not properly published
     */
    public Holder holder;

    public void initialize() {
        holder = new ch3.Holder(42);
    }

}
