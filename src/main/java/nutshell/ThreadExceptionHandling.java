package nutshell;

public class ThreadExceptionHandling {

    private Thread handledThread;

    public ThreadExceptionHandling() {
        this.handledThread =  new Thread(() -> {throw new UnsupportedOperationException();});
    }

    public void setThreadName(String name) {
        handledThread.setName(name);
    }

    public void setUncaughtExceptionHandler() {
        handledThread.setUncaughtExceptionHandler((t, e) -> {
            System.err.printf("Exception in thread %d '%s':" +
                    "%s at line %d of %s%n",
            t.getId(),
            t.getName(),
            e.toString(),
            e.getStackTrace()[0].getLineNumber(),
            e.getStackTrace()[0].getFileName()); });

        handledThread.start();
    }

    public static void main(String[] args) {
        ThreadExceptionHandling the = new ThreadExceptionHandling();
        the.setThreadName("test thread");
        the.setUncaughtExceptionHandler();
    }



}
