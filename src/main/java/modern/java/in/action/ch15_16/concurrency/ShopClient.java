package modern.java.in.action.ch15_16.concurrency;

import java.util.List;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ShopClient {

    private static final double DEFAULT_RATE = 1.00;

    private final List<Shop> shops = List.of(new Shop("Best Price"),
                               new Shop("Let's save Big"),
                               new Shop("MyFavoriteShop"),
                               new Shop("Buy it all"),
                               new Shop("ShopEasy"));

    private final Executor executor = Executors.newFixedThreadPool(Math.min(shops.size(), 100),
            (Runnable r) -> {
                Thread t = new Thread(r);
                t.setDaemon(true);
                return t;
            });

    public List<String> findPrices(String product) {
        return shops.parallelStream()
                .map(shop -> String.format("%s price is %s",
                        shop.getName(), shop.getPrice(product)))
                .collect(toList());
    }

    public List<String> findPricesWithDiscount(String product) {
        return shops.stream()
                .map(shop -> shop.getPrice(product))
                .map(Quote::parse)
                .map(Discount::applyDiscount)
                .collect(toList());
    }

    public List<String> findPricesCompletableFuture(String product) {
        List<CompletableFuture<String>> priceFutures = shops.parallelStream()
                .map(shop -> CompletableFuture.supplyAsync
                        (() ->
                        shop.getName() + " price is " + shop.getPrice(product), executor))
                .collect(toList());

        return priceFutures.stream().map(CompletableFuture::join).collect(toList());
    }

    public List<String> findPricesWithDiscountCompletableFuture(String product) {
        List<CompletableFuture<String>> priceFutures =
                shops.stream()
                        .map(shop -> CompletableFuture.supplyAsync(
                            () -> shop.getPrice(product), executor))
                        .map(future -> future.thenApply(Quote::parse))
                        .map(future -> future.thenCompose(quote ->
                        CompletableFuture.supplyAsync(
                                () -> Discount.applyDiscount(quote), executor)))
                        .collect(toList());
        return priceFutures.stream()
                .map(CompletableFuture::join)
                .collect(toList());
    }

    public Stream<CompletableFuture<String>> findPricesStream(String product) {
        return shops.stream()
                .map(shop -> CompletableFuture.supplyAsync(
                        () -> shop.getPrice(product), executor))
                .map(future -> future.thenApply(Quote::parse))
                .map(future -> future.thenCompose(quote ->
                        CompletableFuture.supplyAsync(
                                () -> Discount.applyDiscount(quote), executor)));
    }

    private Future<Double> getFuturePriceInUSD(String product, Shop shop, ExchangeService exchangeService) {
        return CompletableFuture.supplyAsync(() -> Double.valueOf(shop.getPrice(product)))
                .thenCombine(
                        CompletableFuture.supplyAsync(
                                () -> exchangeService.getRate("EUR", "USD"))
                        .completeOnTimeout(DEFAULT_RATE, 1, TimeUnit.SECONDS),
                        (price, rate) -> price * rate
                ).orTimeout(3, TimeUnit.SECONDS);
    }

    private void listShops(Supplier<String> supplier, Function<String, List<String>> function) {
        long start = System.nanoTime();
        System.out.println(function.apply(supplier.get()));
        long duration = (System.nanoTime() - start) / 1_000_000;
        System.out.println("Done in " + duration + " msecs");
    }

    public static void main(String[] args) {
        String product = "Test product";
        ShopClient shopClient = new ShopClient();

        shopClient.listShops(() -> product, p -> shopClient.findPrices(product));
        shopClient.listShops(() -> product, p -> shopClient.findPricesCompletableFuture(product));
        shopClient.listShops(() -> product, p -> shopClient.findPricesWithDiscount(product));
        shopClient.listShops(() -> product, p -> shopClient.findPricesWithDiscountCompletableFuture(product));

        System.out.println("----------Final method with streaming futures");

        long start = System.nanoTime();
        CompletableFuture[] futures = shopClient.findPricesStream("myPhone27S")
                .map(f -> f.thenAccept(
                        s -> System.out.println(s + " (done in " +
                                ((System.nanoTime() - start) / 1_000_000) + " msecs)")))
                .toArray(CompletableFuture[]::new);
        CompletableFuture.allOf(futures).join();
        System.out.println("All shops have now responded in "
                + ((System.nanoTime() - start) / 1_000_000) + " msecs");
    }
}
