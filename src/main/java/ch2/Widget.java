package ch2;

/**
 * Code that would deadlock if intrinsic locks were not reentrant
 */

public class Widget {

    public synchronized void doSomething() {
        System.out.println("I am doing something");
    }
}

class LoggingWidget extends Widget {

    public synchronized void doSomething() {
        System.out.println(toString() + ": calling doSomething");
        super.doSomething();
    }
}
