package ch1;

import net.jcip.annotations.NotThreadSafe;

@NotThreadSafe
public class UnsafeSequence {
    private int value;

    /** Return unique value */
    public int getNext() {
        return value++;
    }
}
